server
{
	listen 80;
	server_name _;
    root /var/www/html;
	index index.php;

	create_full_put_path  on;
    client_max_body_size 10G;
	fastcgi_buffers 64 4K;
    
    add_header Strict-Transport-Security "max-age=15552000; includeSubDomains; preload" always;

	location / {
		try_files $uri $uri/ =404;
		rewrite ^/caldav(.*)$ /remote.php/caldav$1 redirect;
		rewrite ^/carddav(.*)$ /remote.php/carddav$1 redirect;
		rewrite ^/webdav(.*)$ /remote.php/webdav$1 redirect;
		rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
		rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json last;
		rewrite ^/.well-known/carddav /remote.php/carddav/ redirect;
		rewrite ^/.well-known/caldav /remote.php/caldav/ redirect;
		rewrite ^(/core/doc/[^\/]+/)$ $1/index.php;
	}

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

	location ~ ^(.+?\.php)(/.*)?$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php-fpm.sock;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	}
	
	location ~ ^/(data|config|\.ht|db_structure\.xml|README) {
                deny all;
        }
	
	location ~ /.well-known {
                allow all;
	}
	
	location ~ ^/(build|tests|config|lib|3rdparty|templates|data)/ {
		deny all;
	}

	location ~ ^/(?:\.|autotest|occ|issue|indie|db_|console) {
		deny all;
	}

	location ~* \/remote\/(?:.*)$ {
		rewrite ^ /remote.php last;
	}
}